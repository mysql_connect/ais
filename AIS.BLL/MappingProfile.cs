﻿using AutoMapper;

namespace AIS.BLL
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<DAL.Entities.User, BLL.Models.User>();
            CreateMap<DAL.Entities.Person, BLL.Models.Person>();
            CreateMap<DAL.Entities.Position, BLL.Models.Position>();
        }
    }
}
