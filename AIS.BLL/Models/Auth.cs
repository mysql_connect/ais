﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AIS.BLL.Models
{
    public class Auth
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Token { get; set; }
    }
}
