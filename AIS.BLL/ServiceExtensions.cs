﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using AutoMapper;

namespace AIS {
    public static class ServiceExtension
    {
        public static void ConfigureDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<DAL.EFContext>(options =>
            {
                options.UseMySql(configuration.GetConnectionString("Default"));
            });
        }

        public static void ConfigureRepositoryWrapper(this IServiceCollection services)
        {
            services.AddScoped<DAL.Repositories.IRepositoryWrapper, DAL.Repositories.RepositoryWrapper>();
        }

        public static void ConfigureAutoMapper(this IServiceCollection services)
        {
            services.AddAutoMapper(typeof(BLL.MappingProfile));
        }

        public static void ConfigureServices(this IServiceCollection services)
        {
            services.AddScoped<BLL.Services.IAuthService, BLL.Services.AuthService>();
            services.AddScoped<BLL.Services.ITestService, BLL.Services.TestService>();
        }
    }
}