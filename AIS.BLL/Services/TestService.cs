﻿using System;
using System.Linq;
using System.Collections.Generic;

using AutoMapper;

using AIS.DAL.Repositories;

namespace AIS.BLL.Services
{
    public interface ITestService
    {
        IEnumerable<Models.User> GetAllUsers();
    }

    public class TestService : ITestService
    {
        private readonly IMapper _mapper;
        private readonly IRepositoryWrapper _wrapper;

        public TestService(IMapper mapper, IRepositoryWrapper wrapper)
        {
            _mapper = mapper;
            _wrapper = wrapper;
        }

        public IEnumerable<Models.User> GetAllUsers()
        {
            return _wrapper.Users.GetAllWithPersonAndPosition().Select(u => _mapper.Map<Models.User>(u)).ToList();
        }
    }
}
