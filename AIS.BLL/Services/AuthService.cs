﻿using System;
using System.Linq;
using System.Text;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;

using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Options;

using AIS.BLL.Models;
using AIS.DAL.Repositories;

namespace AIS.BLL.Services
{
    public interface IAuthService
    {
        Auth Authenticate(string login, string password);
    }

    public class AuthService : IAuthService
    {
        private readonly IRepositoryWrapper _wrapper;
        private readonly AppSettings _appSettings;

        public AuthService(IRepositoryWrapper wrapper, IOptions<AppSettings> appSettings) {
            _wrapper = wrapper;
            _appSettings = appSettings.Value;
        }

        public Auth Authenticate(string login, string password) {
            var user = _wrapper.Users.FindByCondition(u => u.Login == login && u.PasswordHash == password).SingleOrDefault();

            if (user != null) {
                var tokenHandler = new JwtSecurityTokenHandler();

                var key = Encoding.ASCII.GetBytes(_appSettings.Secret);

                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Issuer = _appSettings.Issuer,
                    Audience = _appSettings.Audience,
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim(ClaimTypes.Name, user.Login.ToString()),
                        new Claim(ClaimTypes.Role, user.Role.ToString())
                    }),
                    Expires = DateTime.UtcNow.AddDays(7),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };
                var token = tokenHandler.CreateToken(tokenDescriptor);

                return new Auth { Id = user.Id, Name = user.Login, Token = tokenHandler.WriteToken(token) };
            }

            return null;
        }
    }
}
