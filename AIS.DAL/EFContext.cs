﻿using Microsoft.EntityFrameworkCore;

using AIS.DAL.Entities;

namespace AIS.DAL
{
    public class EFContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Person> People { get; set; }
        public DbSet<Position> Positions { get; set; }
        public DbSet<Division> Divisions { get; set; }
        public DbSet<Subdivision> Subdivisions { get; set; }
        public DbSet<InventoryItem> Inventory { get; set; }

        public EFContext(DbContextOptions options) : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
