﻿using AIS.DAL.Entities;

namespace AIS.DAL.Repositories
{
    public interface IInventoryRepository : IRepositoryBase<InventoryItem> { }
}
