﻿using AIS.DAL.Entities;

namespace AIS.DAL.Repositories
{
    public interface ISubdivisionRepository : IRepositoryBase<Subdivision> { }
}
