﻿using System.Linq;

using AIS.DAL.Entities;

namespace AIS.DAL.Repositories
{
    public interface IUserRepository : IRepositoryBase<User> {
        IQueryable<User> GetAllWithPerson();
        IQueryable<User> GetAllWithPersonAndPosition();
    }
}
