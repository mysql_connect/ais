﻿using AIS.DAL.Entities;

namespace AIS.DAL.Repositories
{
    public interface IPositionRepository : IRepositoryBase<Position> { }
}
