﻿using AIS.DAL.Entities;

namespace AIS.DAL.Repositories
{
    public interface IPersonRepository : IRepositoryBase<Person> { }
}
