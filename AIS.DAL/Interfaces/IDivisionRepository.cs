﻿using AIS.DAL.Entities;

namespace AIS.DAL.Repositories
{
    public interface IDivisionRepository : IRepositoryBase<Division> { }
}
