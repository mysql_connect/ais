﻿using System;

namespace AIS.DAL.Repositories
{
    public interface IRepositoryWrapper
    {
        IUserRepository Users { get; }
        IPersonRepository Persons { get; }
        IPositionRepository Positions { get; }
        IDivisionRepository Divisions { get; }
        ISubdivisionRepository Subdivisions { get; }
        IInventoryRepository Inventory { get; }

        void SaveChanges();
    }
}
