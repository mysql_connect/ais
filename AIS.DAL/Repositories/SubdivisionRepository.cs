﻿using AIS.DAL.Entities;

namespace AIS.DAL.Repositories
{
    public class SubdivisionRepository : RepositoryBase<Subdivision>, ISubdivisionRepository
    {
        public SubdivisionRepository(EFContext context) : base(context) { }
    }
}
