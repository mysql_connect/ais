﻿using System;

namespace AIS.DAL.Repositories
{
    public class RepositoryWrapper : IRepositoryWrapper
    {
        private EFContext _context;

        private IUserRepository _user;
        private IPersonRepository _person;
        private IPositionRepository _position;
        private IDivisionRepository _division;
        private ISubdivisionRepository _subdivision;
        private IInventoryRepository _inventory;

        public IUserRepository Users {
            get {
                if (_user == null)
                {
                    _user = new UserRepository(_context);
                }

                return _user;
            }
        }
        public IPersonRepository Persons {
            get {
                if (_person == null)
                {
                    _person = new PersonRepository(_context);
                }

                return _person;
            }
        }
        public IPositionRepository Positions {
            get {
                if (_position == null)
                {
                    _position = new PositionRepository(_context);
                }

                return _position;
            }
        }
        public IDivisionRepository Divisions {
            get {
                if (_division == null)
                {
                    _division = new DivisionRepository(_context);
                }

                return _division;
            }
        }
        public ISubdivisionRepository Subdivisions {
            get {
                if (_subdivision == null)
                {
                    _subdivision = new SubdivisionRepository(_context);
                }

                return _subdivision;
            }
        }
        public IInventoryRepository Inventory {
            get {
                if (_inventory == null)
                {
                    _inventory = new InventoryRepository(_context);
                }

                return _inventory;
            }
        }

        public RepositoryWrapper(EFContext context)
        {
            _context = context;
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }
    }
}
