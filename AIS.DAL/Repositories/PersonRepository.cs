﻿using AIS.DAL.Entities;

namespace AIS.DAL.Repositories
{
    public class PersonRepository : RepositoryBase<Person>, IPersonRepository
    {
        public PersonRepository(EFContext context) : base(context) { }
    }
}
