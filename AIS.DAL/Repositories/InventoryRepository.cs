﻿using AIS.DAL.Entities;

namespace AIS.DAL.Repositories
{
    public class InventoryRepository : RepositoryBase<InventoryItem>, IInventoryRepository
    {
        public InventoryRepository(EFContext context) : base(context) { }
    }
}
