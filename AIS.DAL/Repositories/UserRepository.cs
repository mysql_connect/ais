﻿using System.Linq;
using Microsoft.EntityFrameworkCore;

using AIS.DAL.Entities;

namespace AIS.DAL.Repositories
{
    public class UserRepository : RepositoryBase<User>, IUserRepository
    {
        public UserRepository(EFContext context) : base(context) { }

        public IQueryable<User> GetAllWithPerson()
        {
            return this.Context.Users.Include(u => u.Person).AsNoTracking();
        }

        public IQueryable<User> GetAllWithPersonAndPosition()
        {
            return this.Context.Users.Include(u => u.Person).Include(u => u.Person.Position).AsNoTracking();
        }
    }
}
