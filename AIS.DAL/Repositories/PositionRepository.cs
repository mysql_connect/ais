﻿using AIS.DAL.Entities;

namespace AIS.DAL.Repositories
{
    public class PositionRepository : RepositoryBase<Position>, IPositionRepository
    {
        public PositionRepository(EFContext context) : base(context) { }
    }
}
