﻿using AIS.DAL.Entities;

namespace AIS.DAL.Repositories
{
    public class DivisionRepository : RepositoryBase<Division>, IDivisionRepository
    {
        public DivisionRepository(EFContext context) : base(context) { }
    }
}
