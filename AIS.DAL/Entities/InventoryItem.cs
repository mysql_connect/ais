﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AIS.DAL.Entities
{
    [Table("Inventory")]
    public class InventoryItem : Entity
    {
        [Key]
        public new Guid Id { get; set; }

        [Required, StringLength(250)]
        public string Name { get; set; }

        public DateTime Date { get; set; } = DateTime.Now;

        [Required]
        public int Number { get; set; }

        [Required]
        public int Amount { get; set; }

        [Required]
        public float Price { get; set; }

        [Required, ForeignKey("Subdivision")]
        public int SubdivisionId { get; set; }
        public Subdivision Subdivision { get; set; }
    }
}
