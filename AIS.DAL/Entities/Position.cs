﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AIS.DAL.Entities
{
    [Table("Positions")]
    public class Position : Entity
    {
        [Required]
        public string Name { get; set; }

        public ICollection<Person> Persons { get; set; }
    }
}
