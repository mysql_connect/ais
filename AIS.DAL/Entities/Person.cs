﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AIS.DAL.Entities
{
    [Table("People")]
    public class Person : Entity
    {
        [Required]
        public string Surname { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Patronymic { get; set; }

        [Required, DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }

        [Required, ForeignKey("Position")]
        public int PositionId { get; set; }
        public Position Position { get; set; }

        public ICollection<Subdivision> Subdivisions { get; set; }

        public ICollection<User> Users { get; set; }
    }
}
