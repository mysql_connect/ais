﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AIS.DAL.Entities
{
    [Table("Subdivisions")]
    public class Subdivision : Entity
    {
        [Required]
        public string Name { get; set; }

        [Required, ForeignKey("Person")]
        public int PersonId { get; set; }
        public Person Person { get; set; }

        [Required, ForeignKey("Division")]
        public int DivisionId { get; set; }
        public Division Division { get; set; }

        public ICollection<InventoryItem> Inventory { get; set; }
    }
}
