﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AIS.DAL.Entities
{
    public enum Roles
    {
        User = 0,
        Moderator = 1,
        Admin = 2
    }

    [Table("Users")]
    public class User : Entity
    {
        [Required, StringLength(100)]
        public string Login { get; set; }

        [Required, StringLength(32)]
        public string PasswordHash { get; set; }

        [Required, ForeignKey("Person")]
        public int PersonId { get; set; }
        public Person Person { get; set; }

        [Required]
        public Roles Role { get; set; }
    }
}
