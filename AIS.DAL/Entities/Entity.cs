﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AIS.DAL.Entities
{
    public class Entity
    {
        [Key]
        public int Id { get; set; }
    }
}
