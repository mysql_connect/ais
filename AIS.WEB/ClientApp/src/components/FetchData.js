import React, { Component } from 'react';

export class FetchData extends Component {
  static displayName = FetchData.name;

  constructor(props) {
    super(props);
    this.state = { users: [], loading: true };
  }

  componentDidMount() {
    this.populateUserData();
  }

  static renderUsersTable(users) {
    return (
      <table className='table table-striped' aria-labelledby="tabelLabel">
        <thead>
          <tr>
            <th>Login</th>
            <th>Full Name</th>
            <th>Phone</th>
            <th>Position</th>
          </tr>
        </thead>
        <tbody>
          {users.map(user =>
            <tr key={user.id}>
              <td>{user.login}</td>
              <td>{user.person.surname} {user.person.name} {user.person.patronymic}</td>
              <td>{user.person.phone}</td>
              <td>{user.person.position.name}</td>
            </tr>
          )}
        </tbody>
      </table>
    );
  }

  render() {
    let contents = this.state.loading
        ? <div className="d-flex justify-content-center">
            <div className="spinner-border text-primary" role="status" style={{ width: '3rem', height: '3rem', margin: '50px 0' }}>
                <span className="sr-only">Loading...</span>
            </div>
        </div>
      : FetchData.renderUsersTable(this.state.users);

    return (
      <div>
        <h1 id="tabelLabel" >Users list</h1>
        <p>This component demonstrates fetching data from the server.</p>
        {contents}
      </div>
    );
  }

  async populateUserData() {
    const response = await fetch('http://localhost:7000/api/Test/GetUsers');
    const data = await response.json();

    this.setState({ users: data, loading: false });
  }
}
