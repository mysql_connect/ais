﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

using AIS.API.Models;
using AIS.BLL.Services;

namespace AIS.API.Controllers
{
    [ApiController, Route("api/[controller]/[action]")]
    public class AccountController : ControllerBase
    {
        private readonly IAuthService _authService;

        public AccountController(IAuthService authService)
        {
            _authService = authService;
        }

        [HttpPost]
        public IActionResult Login([FromBody] LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = _authService.Authenticate(model.Login, model.Password);

                if (result != null)
                {
                    return Ok(result);
                }

                ModelState.TryAddModelError("Login", "The login or password you entered is incorrect!");
            }

            return ValidationProblem(ModelState);
        }

        [HttpGet, Authorize]
        public IActionResult GetLogin()
        {
            return Ok(User.Identity.Name);
        }
    }
}