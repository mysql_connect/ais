﻿using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

using AIS.BLL.Services;

namespace AIS.API.Controllers
{
    [ApiController, Route("api/[controller]/[action]")]
    public class TestController : ControllerBase
    {
        private ITestService _test;

        public TestController(ITestService test)
        {
            _test = test;
        }

        [HttpGet]
        public ActionResult<IEnumerable<BLL.Models.User>> GetUsers() {
            return _test.GetAllUsers().ToList();
        }
    }
}